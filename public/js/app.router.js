'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider',
      function ($stateProvider,   $urlRouterProvider) {

          $urlRouterProvider
              .otherwise('/home/app');
          $stateProvider
              .state('home', {
                  abstract: true,
                  url: '/home',
                  templateUrl: 'tpl/layout.html'
              })


              .state('items', {
                  abstract: true,
                  url: '/items',
                  templateUrl: 'tpl/layout.html',
                  resolve: {
                    deps: ['uiLoad', function (uiLoad) {
                      return uiLoad.load('js/controllers/items/items.js');
                    }]
                  }
              })
              .state('items.app', {
                  url: '/app',
                  views: {
                      '': {
                          // this shows the 'contact app view' fully loaded
                          // index of all items.
                          // used as the container for all items index components
                          templateUrl: 'tpl/items/app.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  }
              })
              .state('items.app.add', {
                  url: '/add',
                  views: {
                      // within the 'tpl/items/app.html' , this ui-view="addMiniView",
                      // will load the item info and quick add component
                      'addMiniView': {
                          templateUrl: 'tpl/items/add.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  }
              })
              .state('items.app.add.expanded', {
                  url: '/add',
                  views: {
                      '': {
                          templateUrl: 'tpl/items/app.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  }
              })



              .state('home.mobile', {
                  url: '/mobile',
                  views: {
                      '': {
                          templateUrl: 'tpl/layout_mobile.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_mobile.html'
                      }
                  }
              })
              .state('home.app', {
                  url: '/app',
                  views: {
                      '': {
                          templateUrl: 'tpl/layout_app.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  },
                  resolve: {
                      deps: ['uiLoad',
                        function( uiLoad ){
                          return uiLoad.load( ['js/controllers/tab.js'] );
                      }]
                  }
              })

      }
    ]
  );